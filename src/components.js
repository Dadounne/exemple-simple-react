import React from 'react';

export class HelloWorld extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <h1>Hello, world !!!</h1>
    );
  }
}

export class Todo extends React.Component {
  constructor(props) {
    super(props);

    this.todo = props.todo;
  }

  render() {
    if (this.todo.completed) {
      return <strike>{this.todo.title}</strike>
    } else {
      return <span>{this.todo.title}</span>
    }
  }
}

export class TodoList extends React.Component {
  constructor(props) {
    super(props);

    this.todos = props.todos;
  }

  render() {
    return (
      <ul className="todo_list">
        {this.todos.map(t => (
          <li key={t.id} className='todo_item'>
            <Todo todo={t} />
          </li>
        ))}
      </ul>
    )
  }
}
